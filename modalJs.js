
// Get the modal
//var modal = document.getElementById("myModal");

var modals = document.querySelectorAll('[id^=myModal]');
// Get the button that opens the modal
//var btn = document.getElementById("myBtn");

var btns = document.querySelectorAll('[id^=myBtn]');
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close");

// When the user clicks on the button, open the modal
/*btn.onclick = function() {
    modal.style.display = "block";
}*/

for(let i =0 ; i<btns.length;i++){
    btns[i].onclick = function (){
        //alert("CLICKED");
        const id = btns[i].id;
        const arr = id.split("_");
        for (let j =0;j<modals.length;j++){
            const id2 = modals[j].id;
            const arr2 = id2.split("_");

            if(arr2.at(-1) == arr.at(-1)){
                modals[j].style.display = "block";
            }
        }
    }
}

// When the user clicks on <span> (x), close the modal
for(let i = 0;i<span.length; i++) {
    span[i].onclick = function () {

        for (let j = 0; j < modals.length; j++) {
            modals[j].style.display = "none";
        }
    }
}

// When the user clicks anywhere outside of the modal, close it
