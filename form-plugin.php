<?php
/*
Plugin Name: Form Plugin
 */
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

function wpb_adding_scripts()
{

    wp_register_script('modalJs', plugins_url('modalJs.js', __FILE__), array(), '1.1', true);

    wp_enqueue_script('modalJs');
}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts');


function wpb_adding_styles()
{
    wp_register_style('modalCss', plugins_url('modalCss.css', __FILE__));
    wp_enqueue_style('modalCss');
}
add_action('wp_enqueue_scripts', 'wpb_adding_styles');

function enqueue_custom_admin_style()
{
    wp_register_script('modalJs', plugins_url('modalJs.js', __FILE__), array(), '1.1', true);

    wp_enqueue_script('modalJs');
    wp_register_style('modalCss', plugins_url('modalCss.css', __FILE__));
    wp_enqueue_style('modalCss');
}
add_action('admin_enqueue_scripts', 'enqueue_custom_admin_style');


add_action('gform_after_submission', 'set_post_content', 10, 2);
function set_post_content($entry, $form)
{
    //$custom_data = $form;

    //    echo "<br><br> - - - - - - - - - - - - - - -- - - - - - - - -- - <br><br>";

    $htm = "<button id='myBtn_" . $entry["id"] . "' type='button'>کلیک کنید</button>";
    $htm .= '<!-- The Modal -->
<div id="myModal_' . $entry["id"] . '" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>';

    $htm .= "<table>";
    $htm .= "<tr>";
    $htm .= "<td> عنوان </td>";
    $htm .= "<td> " . get_the_title(get_the_ID()) . " </td>";

    $htm .= "</tr>";

    $custom_data = array();
    $tPrice = 0;
    foreach ($form['fields'] as $fo => $f) {
        $id = $f->id;
        foreach ($entry as $en => $e) {
            if ($en == $id && $e != null) {
                $htm .= "<tr>";
                $htm .= "<td> " . $f->label . " </td>";
                $htm .= "<td> " . $e . " </td>";
                $htm .= "</tr>";
                $tPrice = $e;
            }
        }
    }
    $htm .= "</table>";
    $htm .= "</div></div>";
    //   echo $htm;


    $custom_data['htm'] = $htm;
    $custom_data['newPrice'] = $tPrice;
    WC()->cart->add_to_cart(get_the_ID(), '1', '0', array(), $custom_data);
}






add_action('gform_after_submission', 'set_post_content', 10, 2);
function set_post_content($entry, $form)
{
    $text = "سلام%20من%20کالا%20با%20مشخصات%20زیر%20را%20میخواهم.%0A";
    foreach ($form['fields'] as $fo => $f) {
        $id = $f->id;
        foreach ($entry as $en => $e) {
            if ($en == $id && $e != null) {
                $text .= $f->label . ":%20" . $e . "%0A";
            }
        }
    }
    $htm = "<script type='text/javascript'> window.open('https://api.whatsapp.com/send?phone=" . get_option('wa_order_number') . "&text=" . $text . "'); </script>";

    echo $htm;
}

add_action('admin_menu', 'wa_plugin_setup_menu');

function wa_plugin_setup_menu()
{
    add_menu_page('WA Plugin Page', 'WA Plugin', 'manage_options', 'wa-plugin', 'wa_init');
}

function wa_init()
{
    if (isset($_POST['btn-wa-submit'])) {
        update_option('wa_order_number', $_POST['wa-number']);
    }
?>
    <form method='POST'>
        <h4>تغییر شماره واتساپ</h4>
        <input class="form-control" placeholder="با +۹۸ شروع کنید" value=" <?php get_option('wa_order_number') ?> " name="wa-number" />
        <button type="submit" name="btn-wa-submit" class="btn btn-success">ثبت</button>
    </form>
<?php
}







add_action('woocommerce_before_calculate_totals', 'custom_cart_item_price', 30, 1);
function custom_cart_item_price($cart)
{
    //    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
    //        return;

    //    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
    //        return;

    $cart_items = $cart->cart_contents;
    foreach ($cart_items as $cart_item) {
        //print_r( $cart_item);
        if (@isset($cart_item['newPrice']))
            $cart_item['data']->set_price($cart_item['newPrice']);
    }
}

/*function new_add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
    $cart_item_data['name'] = "JOHN";
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'new_add_cart_item_data', 10, 3 );
*/
function new_get_item_data($item_data, $cart_item_data)
{
    // echo "<br>";
    //print_r($cart_item_data);
    if (isset($cart_item_data['htm'])) {
        $item_data[] = array(
            'key' => __('تمام اطلاعات'),
            'value' => __($cart_item_data['htm'])
        );
    }
    return $item_data;
}
add_filter('woocommerce_get_item_data', 'new_get_item_data', 10, 2);


function new_checkout_create_order_line_item($item, $cart_item_key, $values, $order)
{
    if (isset($values['htm'])) {
        $item->add_meta_data(
            __('تمام اطلاعات'),
            __($values['htm']),
            true
        );
    }
}
add_action('woocommerce_checkout_create_order_line_item', 'new_checkout_create_order_line_item', 10, 4);


?>